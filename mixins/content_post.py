import json

from django.shortcuts import render

from helpers.color_distribution import color_distribution, COLORS
from helpers.date_time_encoder import DateTimeEncoder
from med.models import Category, Photo, Article, Video


class ContentPostMixin:
    def append_data(self, ph, data):
        if self.model == Photo:
            for pos in ph:
                item = {
                    'pk': pos.pk,
                    'title': pos.title,
                    'image': pos.image
                }
                data.append(item)

        elif self.model == Article:
            for pos in ph:
                item = {
                    'pk': pos.pk,
                    'title': pos.title,
                    'subtitle': pos.subtitle,
                    'picture': pos.picture,
                    'body': pos.body
                }
                data.append(item)

        elif self.model == Video:
            for pos in ph:
                item = {
                    'pk': pos.pk,
                    'title': pos.title,
                    'description': pos.description,
                    'picture': pos.picture,
                    'video_link': pos.video_link
                }
                data.append(item)

        return data

    def get_html(self):
        html = ''

        if self.model == Photo:
            html = 'photo/photo_list.html'
        elif self.model == Article:
            html = 'article/articles_list.html'
        elif self.model == Video:
            print('+')
            html = 'video/videos.html'

        return html

    def filter(self, category, ph):
        if self.model == Photo:
            ph = Photo.objects.filter(categories=category.id)
        elif self.model == Article:
            ph = Article.objects.filter(categories=category.id)
        elif self.model == Video:
            ph = Video.objects.filter(categories=category.id)

        return ph

    def post(self, request, *args, **kwargs):
        data = []
        context = {}
        response = None
        not_found = None
        ph = None
        html = self.get_html()
        search_value = self.request.POST.get('search')
        category = Category.objects.filter(title__icontains=search_value)[0]
        if category:
            ph = self.filter(category, ph)

        if ph and len(search_value) > 0:
            if ph:
                objects = self.append_data(ph, data)

                response = objects
            else:
                not_found = 'Not found...'

        else:
            not_found = 'Not found...'

        context['obj_with_colors'] = color_distribution(Category.objects.all())
        context['colors'] = COLORS
        context['qs_cat'] = json.dumps(list(Category.objects.values()), cls=DateTimeEncoder)
        context['data'] = response
        context['not_found'] = not_found
        context['search_cat'] = category.title

        return render(request, html, context=context)
