from django.test import TestCase
from django.contrib.auth.models import User
from accounts.models import Profile
from med.models import Category, Article, Video, Photo, Comment, Answer
from django.db import models


class ProfileModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username='test_user', first_name='test_name', last_name='test_surname', password='test_password')
        Profile.objects.create(
            user=user,
            birth_date='2000-01-01',
            phone='88888888888'
        )

    def test_user_verbose_name(self):
        profile = Profile.objects.first()
        verbose_name = profile._meta.get_field('user').verbose_name
        self.assertEquals(verbose_name, 'Пользователь')

    def test_avatar_null(self):
        profile = Profile.objects.first()
        null = profile._meta.get_field('avatar').null
        self.assertEquals(null, True)

    def test_avatar_blank(self):
        profile = Profile.objects.first()
        blank = profile._meta.get_field('avatar').blank
        self.assertEquals(blank, True)

    def test_avatar_upload_to(self):
        profile = Profile.objects.first()
        upload_to = profile._meta.get_field('avatar').upload_to
        self.assertEquals(upload_to, 'user_pics')

    def test_avatar_verbose_name(self):
        profile = Profile.objects.first()
        verbose_name = profile._meta.get_field('avatar').verbose_name
        self.assertEquals(verbose_name, 'Аватар')

    def test_birth_date_null(self):
        profile = Profile.objects.first()
        null = profile._meta.get_field('birth_date').null
        self.assertEquals(null, False)

    def test_birth_date_blank(self):
        profile = Profile.objects.first()
        blank = profile._meta.get_field('birth_date').blank
        self.assertEquals(blank, False)

    def test_birth_date_verbose_name(self):
        profile = Profile.objects.first()
        verbose_name = profile._meta.get_field('birth_date').verbose_name
        self.assertEquals(verbose_name, 'Дата рождения')

    def test_phone_null(self):
        profile = Profile.objects.first()
        null = profile._meta.get_field('phone').null
        self.assertEquals(null, False)

    def test_phone_blank(self):
        profile = Profile.objects.first()
        blank = profile._meta.get_field('phone').blank
        self.assertEquals(blank, False)

    def test_phone_max_length(self):
        profile = Profile.objects.first()
        max_length = profile._meta.get_field('phone').max_length
        self.assertEquals(max_length, 50)

    def test_black_list_default(self):
        profile = Profile.objects.first()
        default = profile._meta.get_field('black_list').default
        self.assertEquals(default, False)

    def test_categories_blank(self):
        profile = Profile.objects.first()
        blank = profile._meta.get_field('categories').blank
        self.assertEquals(blank, True)

    def test_personal_consultation_default(self):
        profile = Profile.objects.first()
        default = profile._meta.get_field('black_list').default
        self.assertEquals(default, False)

    def test_created_at_auto_now_add(self):
        profile = Profile.objects.first()
        auto_now_add = profile._meta.get_field('created_at').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_instagram_null(self):
        profile = Profile.objects.first()
        null = profile._meta.get_field('instagram').null
        self.assertEquals(null, True)

    def test_instagram_blank(self):
        profile = Profile.objects.first()
        blank = profile._meta.get_field('instagram').blank
        self.assertEquals(blank, True)

    def test_instagram_max_length(self):
        profile = Profile.objects.first()
        max_length = profile._meta.get_field('instagram').max_length
        self.assertEquals(max_length, 100)

    def test_instagram_verbose_name(self):
        profile = Profile.objects.first()
        verbose_name = profile._meta.get_field('instagram').verbose_name
        self.assertEquals(verbose_name, 'Инстаграм')

    def test_is_paid_default(self):
        profile = Profile.objects.first()
        default = profile._meta.get_field('is_paid').default
        self.assertEquals(default, False)

    def test_is_admin_default(self):
        profile = Profile.objects.first()
        default = profile._meta.get_field('is_admin').default
        self.assertEquals(default, False)


class CategoryModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Category.objects.create(title='test category')

    def test_title_label(self):
        category = Category.objects.get(id=1)
        field_label = category._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Название категории')

    def test_title_max_length(self):
        category = Category.objects.get(id=1)
        max_length = category._meta.get_field('title').max_length
        self.assertEquals(max_length, 200)

    def test_is_main_default(self):
        category = Category.objects.first()
        default = category._meta.get_field('is_main').default
        self.assertEquals(default, False)

    def test_main_category_null(self):
        category = Category.objects.first()
        null = category._meta.get_field('main_category').null
        self.assertEquals(null, True)

    def test_main_category_blank(self):
        category = Category.objects.first()
        blank = category._meta.get_field('main_category').blank
        self.assertEquals(blank, True)

    # def test_main_category_related_name(self):
    #     category = Category.objects.first()
    #     related_name = category._meta.get_field('main_category').related_name
    #     self.assertEquals(related_name, 'subcategories')
    #
    # def test_main_category_on_delete(self):
    #     category = Category.objects.first()
    #     on_delete = category._meta.get_field('main_category').on_delete
    #     self.assertEquals(on_delete, models.CASCADE)

    def test_is_paid_default(self):
        category = Category.objects.first()
        default = category._meta.get_field('is_paid').default
        self.assertEquals(default, False)

    def test_category_str(self):
        category = Category.objects.first()
        expected_object_name = category.title
        self.assertEquals(expected_object_name, str(category))


class ArticleModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Article.objects.create(
            title='test article title',
            subtitle='test article title test article title test article title',
            body='test article title test article title test article title test '
                 'article title test article title test article title test article '
                 'title test article title test article title test article title '
                 'test article title test article title',
        )

    def test_title_max_length(self):
        article = Article.objects.get(id=1)
        max_length = article._meta.get_field('title').max_length
        self.assertEquals(max_length, 1500)

    def test_title_unique(self):
        article = Article.objects.first()
        unique = article._meta.get_field('title').unique
        self.assertEquals(unique, True)

    def test_created_at_auto_now_add(self):
        article = Article.objects.first()
        auto_now_add = article._meta.get_field('created_at').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_created_at_verbose_name(self):
        article = Article.objects.first()
        verbose_name = article._meta.get_field('created_at').verbose_name
        self.assertEquals(verbose_name, 'Дата создания')

    def test_updated_at_auto_now(self):
        article = Article.objects.first()
        auto_now = article._meta.get_field('updated_at').auto_now
        self.assertEquals(auto_now, True)

    def test_updated_at_verbose_name(self):
        article = Article.objects.first()
        verbose_name = article._meta.get_field('updated_at').verbose_name
        self.assertEquals(verbose_name, 'Дата редактирования')

    def test_subtitle_max_length(self):
        article = Article.objects.get(id=1)
        max_length = article._meta.get_field('subtitle').max_length
        self.assertEquals(max_length, 1000)

    def test_subtitle_null(self):
        article = Article.objects.first()
        null = article._meta.get_field('subtitle').null
        self.assertEquals(null, True)

    def test_subtitle_verbose_name(self):
        article = Article.objects.first()
        verbose_name = article._meta.get_field('subtitle').verbose_name
        self.assertEquals(verbose_name, 'Подзаголовок')

    def test_picture_upload_to(self):
        article = Article.objects.first()
        upload_to = article._meta.get_field('picture').upload_to
        self.assertEquals(upload_to, 'article_pics')

    def test_picture_verbose_name(self):
        article = Article.objects.first()
        verbose_name = article._meta.get_field('picture').verbose_name
        self.assertEquals(verbose_name, 'Картинка')

    def test_body_verbose_name(self):
        article = Article.objects.first()
        verbose_name = article._meta.get_field('body').verbose_name
        self.assertEquals(verbose_name, 'Тело статьи')

    def test_article_str(self):
        article = Article.objects.first()
        expected_object_name = f'{article.title}, {article.created_at}'
        self.assertEquals(expected_object_name, str(article))


class PhotoModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Category.objects.create(title='test category')
        photo = Photo.objects.create(
            title='test_photo',
            image='test/tes_photo.png'
        )
        photo.categories.set(Category.objects.all())

    def test_title_length(self):
        photo = Photo.objects.first()
        max_length = photo._meta.get_field('title').max_length
        self.assertEquals(max_length, 1500)

    def test_title_unique(self):
        photo = Photo.objects.first()
        unique = photo._meta.get_field('title').unique
        self.assertEquals(unique, True)

    def test_created_at_auto_now_add(self):
        photo = Photo.objects.first()
        auto_now_add = photo._meta.get_field('created_at').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_created_at_verbose_name(self):
        photo = Photo.objects.first()
        verbose_name = photo._meta.get_field('created_at').verbose_name
        self.assertEquals(verbose_name, 'Дата создания')

    def test_updated_at_auto_now(self):
        photo = Photo.objects.first()
        auto_now = photo._meta.get_field('updated_at').auto_now
        self.assertEquals(auto_now, True)

    def test_updated_at_verbose_name(self):
        photo = Photo.objects.first()
        verbose_name = photo._meta.get_field('updated_at').verbose_name
        self.assertEquals(verbose_name, 'Дата редактирования')

    def test_image_null(self):
        photo = Photo.objects.first()
        null = photo._meta.get_field('image').null
        self.assertEquals(null, False)

    def test_image_blank(self):
        photo = Photo.objects.first()
        blank = photo._meta.get_field('image').blank
        self.assertEquals(blank, False)

    def test_image_upload_to(self):
        photo = Photo.objects.first()
        upload_to = photo._meta.get_field('image').upload_to
        self.assertEquals(upload_to, 'image_content')

    def test_image_verbose_name(self):
        photo = Photo.objects.first()
        verbose_name = photo._meta.get_field('image').verbose_name
        self.assertEquals(verbose_name, 'Фото')

    def test_photo_str(self):
        photo = Photo.objects.first()
        expected_object_name = photo.title
        self.assertEquals(expected_object_name, str(photo))


class VideoModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Category.objects.create(title='test category')
        video = Video.objects.create(
            title='test_video',
            picture='test_picture',
            description='test_video_description',
            video_link='https://test.com',
        )
        video.categories.set(Category.objects.all())

    def test_title_length(self):
        video = Video.objects.first()
        max_length = video._meta.get_field('title').max_length
        self.assertEquals(max_length, 1500)

    def test_title_unique(self):
        video = Video.objects.first()
        unique = video._meta.get_field('title').unique
        self.assertEquals(unique, True)

    def test_created_at_auto_now_add(self):
        video = Video.objects.first()
        auto_now_add = video._meta.get_field('created_at').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_created_at_verbose_name(self):
        video = Video.objects.first()
        verbose_name = video._meta.get_field('created_at').verbose_name
        self.assertEquals(verbose_name, 'Дата создания')

    def test_updated_at_auto_now(self):
        video = Video.objects.first()
        auto_now = video._meta.get_field('updated_at').auto_now
        self.assertEquals(auto_now, True)

    def test_updated_at_verbose_name(self):
        video = Video.objects.first()
        verbose_name = video._meta.get_field('updated_at').verbose_name
        self.assertEquals(verbose_name, 'Дата редактирования')

    def test_picture_null(self):
        video = Video.objects.first()
        null = video._meta.get_field('picture').null
        self.assertEquals(null, True)

    def test_picture_blank(self):
        video = Video.objects.first()
        blank = video._meta.get_field('picture').blank
        self.assertEquals(blank, True)

    def test_picture_verbose_name(self):
        video = Video.objects.first()
        verbose_name = video._meta.get_field('picture').verbose_name
        self.assertEquals(verbose_name, 'Картинка')

    def test_description_max_length(self):
        video = Video.objects.first()
        max_length = video._meta.get_field('description').max_length
        self.assertEquals(max_length, 2000)

    def test_description_null(self):
        video = Video.objects.first()
        null = video._meta.get_field('description').null
        self.assertEquals(null, True)

    def test_description_blank(self):
        video = Video.objects.first()
        blank = video._meta.get_field('description').blank
        self.assertEquals(blank, True)

    def test_description_verbose_name(self):
        video = Video.objects.first()
        verbose_name = video._meta.get_field('description').verbose_name
        self.assertEquals(verbose_name, 'Описание')

    def test_video_link_max_length(self):
        video = Video.objects.first()
        max_length = video._meta.get_field('video_link').max_length
        self.assertEquals(max_length, 10000)

    def test_video_link_verbose_name(self):
        video = Video.objects.first()
        verbose_name = video._meta.get_field('video_link').verbose_name
        self.assertEquals(verbose_name, 'Ссылка видео')

    def test_video_link_unique(self):
        video = Video.objects.first()
        unique = video._meta.get_field('video_link').unique
        self.assertEquals(unique, True)

    # def test_categories_to(self):
    #     video = Video.objects.first()
    #     to = video._meta.get_field('categories').to
    #     self.assertEquals(to, 'med.Category')

    def test_video_str(self):
        video = Video.objects.first()
        expected_object_name = video.title
        self.assertEquals(expected_object_name, str(video))


class CommentMotelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username='test_user', password='test_password')
        Comment.objects.create(
            author=user,
            body='test_comment'
        )

    def test_article_null(self):
        comment = Comment.objects.first()
        null = comment._meta.get_field('article').null
        self.assertEquals(null, True)

    def test_article_blank(self):
        comment = Comment.objects.first()
        blank = comment._meta.get_field('article').blank
        self.assertEquals(blank, True)

    def test_photo_null(self):
        comment = Comment.objects.first()
        null = comment._meta.get_field('photo').null
        self.assertEquals(null, True)

    def test_photo_blank(self):
        comment = Comment.objects.first()
        blank = comment._meta.get_field('photo').blank
        self.assertEquals(blank, True)

    def test_video_null(self):
        comment = Comment.objects.first()
        null = comment._meta.get_field('video').null
        self.assertEquals(null, True)

    def test_video_blank(self):
        comment = Comment.objects.first()
        blank = comment._meta.get_field('video').blank
        self.assertEquals(blank, True)

    def test_author_verbose_name(self):
        comment = Comment.objects.first()
        verbose_name = comment._meta.get_field('author').verbose_name
        self.assertEquals(verbose_name, 'Автор')

    def test_body_max_length(self):
        comment = Comment.objects.first()
        max_length = comment._meta.get_field('body').max_length
        self.assertEquals(max_length, 2000)

    def test_body_null(self):
        comment = Comment.objects.first()
        null = comment._meta.get_field('body').null
        self.assertEquals(null, False)

    def test_body_blank(self):
        comment = Comment.objects.first()
        blank = comment._meta.get_field('body').blank
        self.assertEquals(blank, False)

    def test_body_verbose_name(self):
        comment = Comment.objects.first()
        verbose_name = comment._meta.get_field('body').verbose_name
        self.assertEquals(verbose_name, 'Коментарий')

    def test_is_anonymous_default(self):
        comment = Comment.objects.first()
        default = comment._meta.get_field('is_anonymous').default
        self.assertEquals(default, False)

    def test_is_question_default(self):
        comment = Comment.objects.first()
        default = comment._meta.get_field('is_question').default
        self.assertEquals(default, False)

    def test_is_review_default(self):
        comment = Comment.objects.first()
        default = comment._meta.get_field('is_review').default
        self.assertEquals(default, False)

    def test_is_moderated_default(self):
        comment = Comment.objects.first()
        default = comment._meta.get_field('is_moderated').default
        self.assertEquals(default, False)

    def test_is_deleted_default(self):
        comment = Comment.objects.first()
        default = comment._meta.get_field('is_deleted').default
        self.assertEquals(default, False)

    def test_created_at_auto_now_add(self):
        comment = Comment.objects.first()
        auto_now_add = comment._meta.get_field('created_at').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_created_at_verbose_name(self):
        comment = Comment.objects.first()
        verbose_name = comment._meta.get_field('created_at').verbose_name
        self.assertEquals(verbose_name, 'Дата создания')

    def comment_str(self):
        comment = Comment.objects.first()
        expected_object_name = f'{comment.id}. {comment.author}'
        self.assertEquals(expected_object_name, str(comment))


class AnswerModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username='test_user', password='test_password')
        comment = Comment.objects.create(author=user, body='test_comment')
        Answer.objects.create(comment=comment, author=user, body='test_answer')

    def test_comment_verbose_name(self):
        answer = Answer.objects.first()
        verbose_name = answer._meta.get_field('comment').verbose_name
        self.assertEquals(verbose_name, 'Ответ')

    def test_author_verbose_name(self):
        answer = Answer.objects.first()
        verbose_name = answer._meta.get_field('author').verbose_name
        self.assertEquals(verbose_name, 'Автор')

    def test_body_max_length(self):
        answer = Answer.objects.first()
        max_length = answer._meta.get_field('body').max_length
        self.assertEquals(max_length, 2000)

    def test_body_null(self):
        answer = Answer.objects.first()
        null = answer._meta.get_field('body').null
        self.assertEquals(null, False)

    def test_article_blank(self):
        answer = Answer.objects.first()
        blank = answer._meta.get_field('body').blank
        self.assertEquals(blank, False)

    def test_body_verbose_name(self):
        answer = Answer.objects.first()
        verbose_name = answer._meta.get_field('body').verbose_name
        self.assertEquals(verbose_name, 'Ответ')

    def test_created_at_auto_now_add(self):
        answer = Answer.objects.first()
        auto_now_add = answer._meta.get_field('created_at').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_created_at_verbose_name(self):
        answer = Answer.objects.first()
        verbose_name = answer._meta.get_field('created_at').verbose_name
        self.assertEquals(verbose_name, 'Дата создания')

    def test_answer_str(self):
        answer = Answer.objects.first()
        expected_object_name = f'{answer.id} {answer.author}'
        self.assertEquals(expected_object_name, str(answer))
