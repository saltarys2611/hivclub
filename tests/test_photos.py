from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from med.models import Photo


class PhotoTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.list_url = reverse('med:photo_list')
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

    def test_photo_list_page_url(self):
        self.client.login(username='testuser', password='12345')
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'photo/photo_list.html')

    def test_photo_list_GET(self):
        self.client.login(username='testuser', password='12345')
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        photo1 = Photo.objects.create(
            image='static/img/demo.jpg',
            title='Title1',
        )
        photo2 = Photo.objects.create(
            image='static/img/demo.jpg',
            title='Title2',
        )

        photo_list = Photo.objects.all()
        self.assertEqual(photo_list.count(), 2)
