from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from med.models import Article


class ArticleTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.list_url = reverse('med:articles')
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

    def test_articles_list_page_url(self):
        self.client.login(username='testuser', password='12345')
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'article/articles_list.html')

    def test_articles_list_GET(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 302)

        article1 = Article.objects.create(
            picture='static/img/demo.jpg',
            title='Title1',
            subtitle='Subtitle1',
            body='Lorem ipsum'
        )
        article2 = Article.objects.create(
            picture='static/img/demo.jpg',
            title='Title2',
            subtitle='Subtitle2',
            body='Lorem ipsum'
        )

        article_list = Article.objects.all()
        self.assertEqual(article_list.count(), 2)
