from django.views.generic import TemplateView

from med.models import Comment


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reviews'] = Comment.objects.filter(is_review=True)
        return context
