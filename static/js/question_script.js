const questionsURL = "https://hivclub.ru/api/questions/";
const answersURL = "https://hivclub.ru/api/answers/";
const usersURL = 'https://hivclub.ru/api/users/';
const btnSend = $('#btn-send');
const btnAdd = $('#btn-add');
const csrfToken = document.getElementsByName('csrfmiddlewaretoken')[0].value;

let container = document.getElementById('container');
let modal = $('#modal-form');
let modalBtn = $('#modal-btn');
let bgSend = $('#send-bg');
let bgAdd = $('#add-bg');
let sendFlash = $('#send-flash');
let addFlash = $('#add-flash');
let body = $('#body');
let questionId = $('#question-pk');
let content = document.getElementById('content');
let anonymousBtn = document.getElementById('anonymous-btn');
let anonymousBtnText = $('#anonymous-btn-text');
let anonymousBtnBody = $('#anonymous-btn-body');
let anonymous = $('#anonymous');
let user;

window.onload = async() => {
        let userID = $('#user-pk').val();
        user = await ajaxResponse(`${usersURL}${userID}/`);
}

async function ajaxResponse(url, method, data_response) {
    let data = await $.ajax(url, {
        method: method,
        headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrfToken},
        data: JSON.stringify(data_response)
    });
    return data;
}

function addCommentHTML(response) {
    let anonymousIcon = '';
    let containerHTML = container.innerHTML;

    if (response.is_anonymous){
        anonymousIcon = `<span class="anonymous-icon">
                            <i class="bi bi-incognito"></i>
                        </span>`;
    }

    container.innerHTML = `<div class="container">
                                <div class="gap-2 flex-wrapp">
                                    <div class="comment-ring">
                                        <img class="author-no-avatar" src="/static/bg-elements/photo.svg" alt="">
                                    </div>
                                    <div style="width: 80%">
                                        <p style="font-size: 16px; margin-bottom: 1px"><b>${user.first_name} ${user.last_name}</b></p>

                                        <p class="card-subtitle text-muted" style="font-size: 12px">
                                            только что

                                            ---<span class="moderation-text"> на модерации</span>

                                            ${anonymousIcon}
                                        </p>
                                    </div>
                                </div>
                                <p style="font-size: 15px">${response.body}</p>

                            </div>

                            <hr>` + containerHTML;

    hiddeModal(btnSend, bgSend);
    body.val('');
}

function addAnswerCommentHTML(response) {
    let answers = document.getElementById(`answers-${response.comment}`);
    let avatarHTML;
    let adminHTML = ''

    if (user.profile.is_admin){
        adminHTML = '(администратор)'
    }

    if (user.profile.avatar){
        avatarHTML = `<img class="question-author-avatar" src="${user.profile.avatar}" alt="">`
    }else{
        avatarHTML = `<img class="author-no-avatar" src="/static/bg-elements/photo.svg" alt="">`
    }

    answers.innerHTML += `<div class="container">
                                <div style="margin-left: 30px">
                                    <div class="gap-2 flex-wrapp">
                                        <div class="comment-ring-answer">
                                            ${avatarHTML}
                                        </div>
                                        <div style="width: 80%">
                                            <p style="font-size: 16px; margin-bottom: 1px">
                                                <b>${user.first_name} ${user.last_name}</b>
                                                ${adminHTML}
                                            </p>
            
                                            <p class="card-subtitle text-muted" style="font-size: 12px">только что</p>
                                        </div>
                                    </div>
                                    <p style="font-size: 15px">${response.body}</p>
                                </div>
                            </div>`;
    hiddeModal(btnAdd, bgAdd);
    body.val('');
}

function addQuestionHTML(response){
    let containerHTML = container.innerHTML;
    let avatarHTML;

    if (user.profile.avatar){
        avatarHTML = `<img class="question-author-avatar" src="${user.profile.avatar}" alt="">`
    }else{
        avatarHTML = `<div class="question-author-no-avatar">
                          <img class="author-no-avatar" src="/static/bg-elements/photo.svg" alt="">
                      </div>`
    }

    container.innerHTML = `<div class="question-wrapp" data-id="${response.id}">

                                <div class="question-text">
                                    ${response.body}
                                </div>

                                <div class="question-circle circle-${response.id}">
                                    <img src="/static/bg-elements/arrow-down.svg" alt="">
                                </div>
                            </div>

                            <div class="dialog" id="${response.id}">
                                <div id="answers-list-${response.id}">
                                    <div class="question">
                                        <div class="question-author">
                                            ${avatarHTML}
                                            <div class="question-author-name">
                                                ${user.first_name} ${user.last_name}
                                            </div>

                                        </div>

                                        ${response.body}

                                        <div class="question-user-date">
                                            только что
                                        </div>

                                        <div class="line"></div>

                                    </div>
                                </div>

                                <div class="send-question">
                                    <button class="send-question-btn" data-target="addition" data-id="${response.id}">
                                        Дополнить вопрос
                                    </button>
                                </div>
                            </div>` + containerHTML;

    body.val('');

    hiddeModal(btnSend, bgSend);
}

function addAnswerHTML(response){
    let dialog = document.getElementById(`answers-list-${questionId.val()}`);
    console.log(dialog);
    let avatarHTML;

    if (user.profile.avatar){
        avatarHTML = `<img class="question-author-avatar" src="${user.profile.avatar}" alt="">`
    }else{
        avatarHTML = `<div class="question-author-no-avatar">
                          <img class="author-no-avatar" src="/static/bg-elements/photo.svg" alt="">
                      </div>`
    }
    dialog.innerHTML += `<div class="question">

                            <div class="question-author">

                                ${avatarHTML}

                                    <div class="question-author-name">
                                        ${user.first_name} ${user.last_name}
                                    </div>

                                </div>

                                ${response.body}

                                <div class="question-user-date">
                                    Только что
                                </div>

                                <div class="line"></div>

                            </div>`;

    body.val('');
    hiddeModal(btnAdd, bgAdd);
}

function showModal(btn, bg) {
    modal.toggleClass('show-modal hidde-modal');
    btn.toggleClass('move-to-show move-to-hide');
    bg.toggleClass('to-dark to-light');

    modal.removeAttr('hidden');
    bg.removeAttr('hidden');
    btn.removeAttr('hidden');
}

function hiddeModal(btn, bg) {
    modal.toggleClass('show-modal hidde-modal');
    btn.toggleClass('move-to-show move-to-hide');
    bg.toggleClass('to-dark to-light');

    setTimeout(() => {
        modal.attr('hidden', true);
        bg.attr('hidden', true);
        btn.attr('hidden', true);
    }, 800);
}

function anonCheck(response) {
    if (anonymous.attr('value')){
        response['is_anonymous'] = true
    }
    return response
}

function flashingBtn(btn) {
    btn.toggleClass('flashing-on');
    btn.toggleClass('flashing-off');
}

let flashingTimer;

function flashingOff(btn) {
    clearInterval(flashingTimer);
    btn.removeClass('flashing-off');
    if (!btn.hasClass('flashing-on')){
        btn.addClass('flashing-on');
    }
}

modalBtn.click(() => {
    if (anonymousBtn && anonymousBtn.hidden){
        anonymousBtn.hidden = false;
    }

    if (anonymousBtnBody && anonymousBtnBody.hasClass('hidde-anonymous-text')){
        anonymousBtnBody.removeClass();
        anonymous.attr('value', '');
    }

    showModal(btnSend, bgSend);
    flashingTimer = setInterval( () => {flashingBtn(sendFlash)}, 1500);
    setTimeout(() => {
        bgSend.removeClass('disabled');
        btnSend.removeClass('disabled');
        }, 800);
});

bgSend.click(async () => {
    if (!bgSend.hasClass('disabled')) {
        bgSend.addClass('disabled');
        await hiddeModal(btnSend, bgSend);
        flashingOff(sendFlash);
    }
});

bgAdd.click(async () => {
    if (!bgAdd.hasClass('disabled')) {
        bgAdd.addClass('disabled');
        await hiddeModal(btnAdd, bgAdd);
        flashingOff(addFlash);
    }
});

btnSend.click(async () => {
    if (!btnSend.hasClass('disabled')) {
        btnSend.addClass('disabled');
        let question = body.val();

        if (btnSend.data('comment')){
            let contentType = content.dataset['contentType'];
            let contentId = content.value;
            let response = {'body': question, 'author': user.id}
            response[`${contentType}`] = contentId

            await ajaxResponse(questionsURL, 'POST', anonCheck(response)).then(addCommentHTML);
        }
        else {
            await ajaxResponse(questionsURL, 'POST', {'body': question, 'author': user.id, 'is_question': true}).then(addQuestionHTML);
        }
    }
});

btnAdd.click(async () => {
    if (!btnAdd.hasClass('disabled')) {
        btnAdd.addClass('disabled');
        let answer = body.val();
        let question = questionId.val();

        if (btnAdd.data('comment')){
            await ajaxResponse(answersURL, 'POST', {'body': answer, 'author': user.id, 'comment': question}).then(addAnswerCommentHTML);
        }
        else{
            if (user.profile.is_admin){
                await ajaxResponse(`${questionsURL}${question}/`, 'PATCH', {'is_moderated': true});
            } else {
                await ajaxResponse(`${questionsURL}${question}/`, 'PATCH', {'is_moderated': false});
            }

            await ajaxResponse(answersURL, 'POST', {'body': answer, 'author': user.id, 'comment': question}).then(addAnswerHTML);
        }
    }
});

container.onclick = (event) => {
    if (event.target.dataset['target'] === 'addition'){

        if (anonymousBtn && !anonymousBtn.hidden){
            anonymousBtn.hidden = true;
        }

        questionId.attr('value', event.target.dataset['id']);
        flashingTimer = setInterval( () => {flashingBtn(addFlash)}, 1500);
        showModal(btnAdd, bgAdd);
        setTimeout(() => {
            bgAdd.removeClass('disabled');
            btnAdd.removeClass('disabled');
            }, 800);
    }
    else {
        let id = event.target.dataset['id'];
        let dialog = $(`#${id}`);
        let circle = $(`.circle-${id}`);

        if (!circle.hasClass('rotate-up')) {
            circle.addClass('rotate-up');
            circle.removeClass('rotate-down');
        } else {
            circle.removeClass('rotate-up');
            circle.addClass('rotate-down');
        }

        if (dialog.is(':hidden')) {
            dialog.slideDown(300);
        } else {
            dialog.slideUp(300);
        }
    }
}

if (anonymousBtn){
    anonymousBtn.onclick = (event) => {
        anonymousBtnText.slideToggle(250);
        anonymousBtnText.toggleClass('hidde-anonymous-text');
        anonymousBtnText.toggleClass('show-anonymous-text');

        if (!anonymousBtnBody.hasClass('show-anonymous-text')){
            anonymous.attr('value', true);
            anonymousBtnBody.removeClass('hidde-anonymous-text');
            anonymousBtnBody.addClass('show-anonymous-text');
        }
        else{
            anonymous.attr('value', '');
            anonymousBtnBody.removeClass('show-anonymous-text');
            anonymousBtnBody.addClass('hidde-anonymous-text');
        }
    }
}
