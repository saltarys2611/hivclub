const data = window.__INITIALSTATE__;
const rdata = JSON.parse(data.replace(/&quot;/g, '"'));
const csrf = document.getElementsByName('csrfmiddlewaretoken')[0].value;
const colorsArray = colors.replace(/&#x27;|[\[\]']/g, '').split(', ');

let input = document.getElementById('search');
let container = document.getElementById('container');
let inputContentType = document.getElementById('content_type');
let box = document.getElementsByClassName('box')[0];
let circle = document.getElementsByClassName('circle-cat')[0];

let filteredArr = [];


input.addEventListener('focus', ()=> {
    circle.classList.add('rotate-up');
    circle.classList.remove('rotate-down');
    circle.style.pointerEvents = 'auto';
    $(box).slideDown(800);
});


circle.addEventListener('click', () => {
    closeBoxes();
    circle.classList.add('rotate-down');
    circle.classList.remove('rotate-up');
    circle.style.pointerEvents = 'none';
    $(box).slideUp(400);
})


function createSubmitBtn(box, category, color, type, text) {
    let submitBtn = document.createElement('button');
    submitBtn.classList.add('btn-submit');
    submitBtn.type = 'submit';
    submitBtn.name = 'category';
    submitBtn.style.borderColor = color;
    submitBtn.style.marginRight = '3px';
    submitBtn.dataset['contentType'] = type;
    submitBtn.value = category.id

    let btnText = document.createTextNode(text);
    submitBtn.appendChild(btnText);

    box.appendChild(submitBtn);
}


function createSubmitBox(btn, category, color) {
    let submitBox = document.createElement('div');
    submitBox.classList.add('submit-box');
    submitBox.id = category.id;
    submitBox.style.borderColor = color;

    createSubmitBtn(submitBox, category, color, 'videos', 'Видео');
    createSubmitBtn(submitBox, category, color, 'photos', 'Сторис');
    createSubmitBtn(submitBox, category, color, 'articles', 'Публикации');

    btn.appendChild(submitBox);
}


input.addEventListener('keyup', (e) => {
    console.log('+')
    container.innerHTML = '';
    filteredArr = rdata.filter(category=> category['title'].toLowerCase().includes(e.target.value.toLowerCase()));

    if (filteredArr.length > 0){
        filteredArr.map(category=>{
            let rand = Math.floor(Math.random() * colorsArray.length);
            let color = colorsArray[rand];

            if (! category['main_category_id'] && input.dataset['catList']){
                container.innerHTML += `<div class="btn-cat" data-id="${category.id}" style="border-color: ${color}">
                                      ${category.title}   
                                      
                                      <div class="submit-box" id="${category.id}" style="border-color: ${color}">
                                          <button type="submit" class="btn-submit" name="category" style="border-color: ${color}" data-content-type="videos" value="${category.id}">Видео</button>
                                          <button type="submit" class="btn-submit" name="category" style="border-color: ${color}" data-content-type="photos" value="${category.id}">Сторис</button>
                                          <button type="submit" class="btn-submit" name="category" style="border-color: ${color}" data-content-type="articles" value="${category.id}">Публикации</button>
                                      </div>
                                      
                                  </div>
                                  <br>`;
            } else if (! category['main_category_id']) {
                container.innerHTML += `<input type="submit" class="btn-cat sub-cat" name="search" value="${category.title}" style="border-color: ${color}"><br>`;
            }


            if (category['is_main']){
                let subcategoriesBox = document.createElement('div');
                subcategoriesBox.classList.add('subcategories-box');

                filteredArr.map(subcategory => {
                    if (subcategory['main_category_id'] === category['id']){
                        let subcategoryBtn;

                        if (input.dataset['catList']){
                            subcategoryBtn = document.createElement('div');
                            subcategoryBtn.dataset['id'] = subcategory.id;
                            let btnText = document.createTextNode(subcategory['title']);
                            subcategoryBtn.appendChild(btnText);

                            createSubmitBox(subcategoryBtn, subcategory, color);
                        } else {
                            subcategoryBtn = document.createElement('input');
                            subcategoryBtn.type = 'submit';
                            subcategoryBtn.value = subcategory['title'];
                            subcategoryBtn.name = 'search';
                        }

                        subcategoryBtn.classList.add('btn-cat');
                        subcategoryBtn.classList.add('sub-cat');
                        subcategoryBtn.style.borderColor = color;
                        subcategoriesBox.appendChild(subcategoryBtn);
                    }
                })
                container.appendChild(subcategoriesBox);
            }
        })
    } else{
        container.innerHTML = '<b>Not found</b>';
    }
})


function closeBox(box, btn) {
    box.slideUp(300);
    box.removeClass('open');
    setTimeout(() => {
        btn.css('background-color', 'rgba(0, 0, 0, 0)');
        }, 300);
}


function closeBoxes() {
    let openBoxes = $('.open');

    for (const openBox of openBoxes) {
        let openBoxId = openBox.id;
        let openBtn = $(`.btn-cat[data-id="${openBoxId}"]`);

        closeBox($(openBox), openBtn);
    }
}


container.onclick = function (event) {
    let target = event.target;
    let id = target.dataset['id'];
    let submitBox = $(`#${id}`);

    if (submitBox.is(':hidden')) {
        closeBoxes()
        target.style.backgroundColor = target.style['borderColor'];
        submitBox.slideDown(300);
        submitBox.addClass('open');
    } else {
        let jBtn = $(target);
        closeBox(submitBox, jBtn);
    }

    if (target.type === 'submit') {
        inputContentType.value = target.dataset['contentType'];
    }
}