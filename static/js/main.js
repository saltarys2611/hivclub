console.log('hello world')

const url = window.location.href
const searchForm = document.getElementById('search-form');
const searchInput = document.getElementById('search-input');
let resultsBox = document.getElementById('results-box');

const csrf = document.getElementsByName('csrfmiddlewaretoken')[0].value

const sendSearchData = (search_value) => {
    $.ajax({
        type: 'POST',
        url: 'search/',
        data: {
            'csrfmiddlewaretoken': csrf,
            'search_value': search_value
        },
        success: (response) => {
            console.log(11111)
            console.log(response.data)
            console.log(response.data)
            let contentURL = response.data[0]['content_url']
            const data = response.data
            if (Array.isArray(data)) {
                resultsBox.innerHTML = ''
               data.forEach(search_value=> {
                   resultsBox.innerHTML += `
                    <a href="${url}${contentURL}/${search_value.pk}" class="item">
                        <div class="row mt-2 mb-2">
                            <div class="col-10">
                                <h5>${search_value.title}</h5>
                            </div>
                        </div>
                    </a>
                   `
               })
            }else {
                if (searchInput.value.length > 0) {
                    resultsBox.innerHTML = `<b>${data}</b>`
                } else {
                    resultsBox.classList.add('not-visible')
                }
            }
        },
        error: (response) => {
            console.log(response)
        }
    })
}

searchInput.addEventListener('focus', e => {
    console.log(e.target.value)

    if (resultsBox.classList.contains('not-visible')) {
        resultsBox.classList.remove('not-visible')
    }

    sendSearchData(e.target.value)
})