let openMenu = document.getElementById('open');
let closeMenuBtn = document.getElementById('close');
let menu = document.getElementById('menu');
let hideBlock = document.getElementById('hide');
let hideBg = document.getElementById('hide-bg');

openMenu.onclick = () => {
    hideBlock.style.pointerEvents = 'auto';
    hideBg.style.pointerEvents = 'auto';

    menu.classList.toggle('to-hide');
    menu.classList.toggle('to-show');

    hideBg.classList.toggle('to-light');
    hideBg.classList.toggle('to-dark');

    hideBlock.hidden = false;
    hideBg.hidden = false;
};

function closeMenu() {
    hideBlock.style.pointerEvents = 'none';
    hideBg.style.pointerEvents = 'none';

    menu.classList.toggle('to-hide');
    menu.classList.toggle('to-show');

    hideBg.classList.toggle('to-light');
    hideBg.classList.toggle('to-dark');

    setTimeout(() => {hideBlock.hidden = true}, 400);
    setTimeout(() => {hideBg.hidden = true}, 400);
}

hideBlock.onclick = (event) => {
    if (event.target === closeMenuBtn) {
        closeMenu();
    } else if (event.target === hideBlock){
        closeMenu();
    }
}

hideBg.onclick = () => {
    closeMenu();
}
