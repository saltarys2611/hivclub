from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth import views as auth_views
from django.urls import path
from accounts.views.admin_views import (
    PaidUsersListView, UnpaidUsersListView, MedAdmin,
    UsersListView,
)
from accounts.views.profile_views import (
    ProfileView, QuestionsView, ProfileEdit,
    RegisterView, UserPasswordChangeView, RenewSubscriptionView,
    SubscriptionView
)


urlpatterns = [

    # Authentication
    path('accounts/login/', LoginView.as_view(), name='login'),
    path('accounts/logout/', LogoutView.as_view(), name='logout'),
    path('accounts/register/', RegisterView.as_view(), name='signup'),
    path('accounts/<int:pk>/change_password/', UserPasswordChangeView.as_view(), name='user_password_change'),
    path('accounts/password_reset/',
         auth_views.PasswordResetView.as_view(template_name='passwords/password_reset.html'),
         name='password_reset'),
    path('accounts/password_reset_done/',
         auth_views.PasswordResetDoneView.as_view(template_name='passwords/password_reset_done.html'),
         name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='passwords/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('accounts/reset_complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='passwords/password_reset_complete.html'),
         name='password_reset_complete'),

    # Profile
    path('accounts/profile/<int:pk>', ProfileView.as_view(), name='profile'),
    path('accounts/profile/<int:pk>/questions', QuestionsView.as_view(), name='questions'),
    path('accounts/profile/<int:pk>/update', ProfileEdit.as_view(), name='profile_edit'),

    # Admin
    path('site/admin/', MedAdmin.as_view(), name='admin_site'),
    path('users/all', UsersListView.as_view(), name='all_users'),
    path('users/paid', PaidUsersListView.as_view(), name='paid_users'),
    path('users/unpaid', UnpaidUsersListView.as_view(), name='unpaid_users'),

    # Subscription
    path('accounts/renew', RenewSubscriptionView.as_view(), name='renew_subscription'),
    path('accounts/subscription', SubscriptionView.as_view(), name='subscription'),
]
