from django.core.validators import RegexValidator

phone_validator = RegexValidator(
    regex=r'^\+\d{7,15}',
    message="Формат телефона: '+999999999'. Не более 15 цифр.")
