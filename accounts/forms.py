from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from accounts.helpers import phone_validator
from accounts.models import Profile


class MyProfileForm(forms.ModelForm):
    phone = forms.CharField(label='Телефон', required=True)
    birth_date = forms.DateField(label='Дата рождения', required=True)

    class Meta:
        model = Profile
        fields = ['phone', 'birth_date', 'instagram']


class MyUserCreationForm(UserCreationForm):
    password1 = forms.CharField(label="Пароль", strip=False, required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(label="Подтвердите пароль", required=True, widget=forms.PasswordInput,
                                strip=False)
    first_name = forms.CharField(label="Имя", required=True)
    last_name = forms.CharField(label="Фамилия", required=True)
    email = forms.CharField(label="Почта", required=True)
    phone = forms.CharField(label="Телефон", required=True, validators=[phone_validator])
    birth_date = forms.DateField(label="Дата рождения", required=True)
    instagram = forms.CharField(label='Инстаграм', required=True)

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password1")
        password_confirm = cleaned_data.get("password2")
        email = cleaned_data.get('email')
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Пароли не совпадают!')
        if User.objects.filter(email=email).exists():
            raise ValidationError("Указанный email существует")
        return cleaned_data

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = (
            'username', 'password1', 'password2',
            'first_name', 'last_name', 'email')


class PasswordChangeForm(forms.ModelForm):
    old_password = forms.CharField(label="Старый пароль", strip=False, widget=forms.PasswordInput)
    password = forms.CharField(label="Новый пароль", strip=False, widget=forms.PasswordInput)
    password_confirm = forms.CharField(label="Подтвердите пароль", widget=forms.PasswordInput, strip=False)

    def clean_password_confirm(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Пароли не совпадают!')
        return password_confirm

    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')
        if not self.instance.check_password(old_password):
            raise forms.ValidationError('Старый пароль неправильный!')
        return old_password

    def save(self, commit=True):
        user = self.instance
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ['old_password', 'password', 'password_confirm']


class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(label='Имя', required=True)
    last_name = forms.CharField(label='Фамилия', required=True)
    email = forms.CharField(label="Почта", required=True)

    class Meta:
        model = User
        fields = (
            'first_name', 'last_name', 'email')


class ProfileUpdateForm(forms.ModelForm):
    phone = forms.CharField(label='Номер телефона', required=True, validators=[phone_validator])
    birth_date = forms.DateField(label="Дата рождения", required=True)
    instagram = forms.CharField(label='Инстаграм', required=True)

    class Meta:
        model = Profile
        fields = ('phone', 'birth_date', 'instagram', 'avatar')


class ProfileCategoriesForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['categories', ]
