# from django.contrib.auth import get_user_model
# from django.contrib.auth.models import User
# from django.test import TestCase
# from django.urls import reverse
# from selenium.webdriver import Chrome
#
#
# class BaseTest(TestCase):
#     def setUp(self) -> None:
#         self.driver = Chrome()
#
#     def tearDown(self) -> None:
#         self.driver.close()
#
#     def get_url(self, url: str) -> None:
#         self.driver.get(url)
#
#     def check_url(self, url: str) -> None:
#         assert self.driver.current_url == url
#
#     def fill_input_by_name(self, name: str, text: str) -> None:
#         self.driver.find_element_by_name(name).send_keys(text)
#
#     def click_button(self, button: str) -> None:
#         self.driver.find_element_by_css_selector(button).click()
#
#     def see_error_with_text(self, text: str) -> None:
#         error = self.driver.find_element_by_css_selector(text)
#         assert error.text
#
#
# class RegisterTest(TestCase):
#     def setUp(self) -> None:
#         self.first_name = 'testuser_name'
#         self.last_name = 'testuser_lastname'
#         self.username = 'testuser'
#         self.email = 'mail@mail.com'
#         self.password1 = 'poiupoiu'
#         self.password2 = 'poiupoiu'
#
#     def test_register_page_url(self):
#         response = self.client.get('/accounts/register/')
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, template_name='accounts/user_create.html')
#
#     def test_register_user(self):
#         self.test_user = User.objects.create(
#             first_name=self.first_name,
#             last_name=self.last_name,
#             username=self.username,
#             email=self.email,
#             password=self.password1,
#         )
#
#         # self.assertEqual(self.response.status_code, 200)
#
#         users = get_user_model().objects.all()
#         self.assertEqual(users.count(), 1)
#
#
# class ProfileEditTest(TestCase):
#     def setUp(self) -> None:
#         self.test_user = User.objects.create(
#             username='test_username',
#             password='poiupoiu',
#             first_name='test_user_first_name',
#             last_name='test_user_last_name',
#             email='mail@mail.com'
#         )
#
#     def tearDown(self) -> None:
#         self.test_user.delete()
#
#     def test_profile_edit_page_url(self):
#         response = self.client.get(f'/accounts/profile/{self.test_user.id}/update', follow=True)
#         self.assertEqual(response.status_code, 200)
#
#     def test_profile_edit(self) -> None:
#         response = self.client.post(
#             reverse('profile_edit', kwargs={'pk': self.test_user.id}), data={
#                 'username': 'test_user1',
#                 'password': 'poiupoiu',
#                 'first_name': 'John',
#                 'last_name': 'Doe',
#                 'email': 'newmail@mail.com'
#             }, follow=True)
#         self.assertEqual(response.status_code, 200)
#         self.test_user.refresh_from_db()
#         print(self.test_user.first_name)
#         self.assertEqual(self.test_user.first_name, 'John')
#         self.assertEqual(self.test_user.last_name, 'Doe')
