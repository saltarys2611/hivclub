import logging

from django.contrib.auth.models import User
from django.shortcuts import render

from accounts.models import Profile
from med.models import Comment, Article, Video, Photo

from accounts.views.helpers import AdminPermissionsView


logger = logging.getLogger('main')


class MedAdmin(AdminPermissionsView):

    def get(self, request, *args, **kwargs):
        new_comments = []
        new_questions = []
        for comment in Comment.objects.all():
            if not comment.is_question and not comment.is_moderated:
                new_comments.append(comment)
            elif comment.is_question and not comment.is_moderated:
                new_questions.append(comment)

        return render(request, 'profile/med_admin.html', context={
            'articles_count': len(Article.objects.all()),
            'videos_count': len(Video.objects.all()),
            'photos_count': len(Photo.objects.all()),
            'comments_count': len(Comment.objects.filter(is_question=False)),
            'questions_count': len(Comment.objects.filter(is_question=True)),
            'new_questions': len(new_questions),
            'new_comments': len(new_comments),
            'users_count': len(User.objects.all()),
            'paid_users_count': len(Profile.objects.filter(is_paid=True)),
            'unpaid_users_count': len(Profile.objects.filter(is_paid=False)),
        })


class UsersListView(AdminPermissionsView):

    def get(self, request, *args, **kwargs):
        users = Profile.objects.all()
        return render(request, 'profile/users.html', context={
            'users': users,
        })


class PaidUsersListView(AdminPermissionsView):

    def get(self, request, *args, **kwargs):
        paid_users = Profile.objects.filter(is_paid=True)
        return render(request, 'profile/paid_users.html', context={
            'paid_users': paid_users,
        })


class UnpaidUsersListView(AdminPermissionsView):

    def get(self, request, *args, **kwargs):
        unpaid_users = Profile.objects.filter(is_paid=False)
        return render(request, 'profile/unpaid_users.html', context={
            'unpaid_users': unpaid_users,
        })
