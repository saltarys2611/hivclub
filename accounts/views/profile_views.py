import logging

from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse

from django.views.generic import CreateView, UpdateView, DetailView, ListView, TemplateView
from med.helpers import get_age

from accounts.forms import (
    MyUserCreationForm, PasswordChangeForm, ProfileUpdateForm, UserUpdateForm
)
from accounts.views.helpers import AdminPermissionsView

from accounts.models import Profile
from med.models import Category, Comment, Answer


logger = logging.getLogger('main')


class RegisterView(CreateView):
    model = User
    template_name = 'accounts/user_create.html'
    form_class = MyUserCreationForm

    def form_valid(self, form):
        date_birth = self.request.POST.get('birth_date')
        phone = self.request.POST.get('phone')
        instagram = self.request.POST.get('instagram')
        if date_birth == '' or phone == '' or instagram == '':
            return self.form_invalid(form)
        else:
            user = form.save()
            logger.info(f'Пользователь id{user.pk} зарегистрировался.')
            Profile.objects.create(
                user=user,
                birth_date=self.request.POST.get('birth_date'),
                phone=phone,
                personal_consultation=self.request.POST.get('personal_consultation'),
                instagram=instagram
            )
            login(self.request, user)
            return redirect('profile', pk=user.pk)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form, data=self.request.POST))


class UserPasswordChangeView(UserPassesTestMixin, UpdateView):
    model = get_user_model()
    template_name = 'accounts/user_password_change.html'
    form_class = PasswordChangeForm
    context_object_name = 'user_obj'

    def get_success_url(self):
        return reverse('login')

    def test_func(self):
        return self.request.user == self.get_object()


class ProfileView(UserPassesTestMixin, DetailView):
    model = get_user_model()
    context_object_name = 'user_obj'
    template_name = 'profile/profile_view.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data()
        context['categories'] = Category.objects.all()
        context['questions'] = Comment.objects.filter(is_question=True, author=self.object)
        context['answers'] = Answer.objects.all()
        context['profile'] = Profile.objects.all()
        # context['user_id'] = self.kwargs.get('pk')

        age = get_age(self.object.profile.birth_date)
        context['age'] = age
        return context

    def test_func(self):
        return self.get_object() == self.request.user \
               or self.request.user.is_superuser or self.request.user.profile.is_admin


class ProfileEdit(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = UserUpdateForm
    template_name = 'profile/profile_update.html'
    context_object_name = 'user_obj'

    def get(self, request, *args, **kwargs):
        if request.user.pk == kwargs.get('pk'):
            return super(ProfileEdit, self).get(request, *args, **kwargs)
        else:
            wrong_id = kwargs.get('pk')
            logger.warning(f'Пользователь id-{request.user.pk} пытался редактировать профиль пользователя id-{wrong_id}.')
            raise Http404

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
            kwargs['birth_date'] = str(self.get_object().profile.birth_date)
        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()
        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return self.render_to_response(context)

    def get_profile_form(self):
        form_kwargs = {'instance': self.object.profile}
        if self.request.method == 'POST':
            form_kwargs['data'] = self.request.POST
            form_kwargs['files'] = self.request.FILES
        return ProfileUpdateForm(**form_kwargs)

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.object.pk})


class QuestionsView(ListView):
    template_name = 'profile/users_questions.html'
    context_object_name = 'questions'

    def get_queryset(self):
        user_id = self.kwargs.get('pk')
        return Comment.objects.filter(author=user_id, is_question=True)

    def get(self, request, *args, **kwargs):
        if request.user.pk == kwargs.get('pk') or request.user.profile.is_admin:
            return super(QuestionsView, self).get(request, *args, **kwargs)
        else:
            wrong_id = kwargs.get('pk')
            logger.warning(f'Пользователь id-{request.user.pk} пытался просматривать вопросы пользователя id-{wrong_id}.')
            raise Http404

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(QuestionsView, self).get_context_data()
        context['answers'] = Answer.objects.all()
        context['user_obj'] = User.objects.get(id=self.kwargs.get('pk'))
        return context


class RenewSubscriptionView(LoginRequiredMixin, TemplateView):
    template_name = 'profile/renew_subscription.html'


class SubscriptionView(TemplateView):
    template_name = 'profile/subscription.html'
