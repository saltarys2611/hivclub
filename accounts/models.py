from django.contrib.auth import get_user_model
from django.db import models
from django.contrib.auth.models import AbstractUser


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), related_name='profile',
                                on_delete=models.CASCADE, verbose_name='Пользователь')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Аватар')
    birth_date = models.DateField(null=False, blank=False, verbose_name="Дата рождения")
    phone = models.CharField(max_length=50, null=False, blank=False, verbose_name='Телефон')
    black_list = models.BooleanField(default=False)
    categories = models.ManyToManyField('med.Category', blank=True, related_name='favorite_categories')
    personal_consultation = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)
    instagram = models.CharField(blank=True, max_length=100, null=True, verbose_name='Инстаграм')
    is_paid = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    def str(self):
        return self.user.get_full_name() + "'s Profile"

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'