from django import forms
from med.models import Category, Comment, Video, Article, Photo, Answer


class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ('title', 'description', 'video_link', 'categories', 'picture')


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('title', 'subtitle', 'picture', 'body', 'categories')


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title', 'main_category', 'is_paid']


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ['title', 'image', 'categories']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['body', ]


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['body', 'comment']


class CommentModerationForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['is_review', 'is_question', 'is_deleted', ]
