from django.urls import path

from med.views.comment_views import CommentDeleteView, CommentListView, CommentModerationView, QuestionListView
from med.views.info_page import AboutMeView, OnlineConsultationView, OfflineConsultationView, AboutClubView
from med.views.photo_views import (
    PhotoCreateView, PhotoListView, PhotoDeleteView,
    PhotoDetailView, PhotoUpdateView
)

from med.views.search_view import search_view
from med.views.video_views import (
    VideoDetailView, Videos,
    AdminVideosList, VideoUpdate, VideoDeleteView
)

from med.views.article_views import (
    ArticleDeleteView, ArticleUpdateView, ArticleCreateView,
    ArticleDetailView, ArticleListView
)

from med.views.category_views import (
    CategoryListView, CategoryCreateView, CategoryDeleteView,
    CategoryEditView, CategoryArticleListView, CategoryPhotoListView,
    CategoryVideoListView
)


app_name = 'med'

urlpatterns = [
    # Video
    path('video/<int:pk>/', VideoDetailView.as_view(), name='video_detail'),
    # path('video/create/', VideoCreateView.as_view(), name='video_create'),
    path('videos/unpublished/', AdminVideosList.as_view(), name='videos_unpublished'),
    path('videos/', Videos.as_view(), name='videos'),
    path('video/<int:pk>/update/', VideoUpdate.as_view(), name='video_update'),
    path('video/<int:pk>/delete/', VideoDeleteView.as_view(), name='video_delete'),

    # Article
    path('article/create/', ArticleCreateView.as_view(), name='article_create'),
    path('article/<int:pk>/', ArticleDetailView.as_view(), name='article_detail'),
    path('articles/', ArticleListView.as_view(), name='articles'),
    path('article/<int:pk>/delete', ArticleDeleteView.as_view(), name='article_delete'),
    path('article/<int:pk>/update', ArticleUpdateView.as_view(), name='article_update'),

    # Category
    path('categories/', CategoryListView.as_view(), name='categories'),
    path('category/create/', CategoryCreateView.as_view(), name='category_create'),
    path('category/<int:pk>/delete/', CategoryDeleteView.as_view(), name='category_delete'),
    path('category/<int:pk>/edit', CategoryEditView.as_view(), name='category_update'),
    path('category/<int:pk>/article/list', CategoryArticleListView.as_view(), name='category_article_list'),
    path('category/<int:pk>/photo/list', CategoryPhotoListView.as_view(), name='category_photo_list'),
    path('category/<int:pk>/video/list', CategoryVideoListView.as_view(), name='category_video_list'),

    # Photo
    path('photo/create', PhotoCreateView.as_view(), name='photo_create'),
    path('photo/list', PhotoListView.as_view(), name='photo_list'),
    path('photo/<int:pk>/delete', PhotoDeleteView.as_view(), name='photo_delete'),
    path('photo/<int:pk>/', PhotoDetailView.as_view(), name='photo_detail'),
    path('photo/<int:pk>/update', PhotoUpdateView.as_view(), name='photo_update'),

    # Comment
    path('comment/<int:pk>/delete', CommentDeleteView.as_view(), name='comment_delete'),
    path('comments/list', CommentListView.as_view(), name='comment_list'),
    path('questions/list', QuestionListView.as_view(), name='questions_list'),
    path('comment/<int:pk>/detail', CommentModerationView.as_view(), name='comment_detail'),

    # Others
    path('search/', search_view, name='search'),
    path('about_me/', AboutMeView.as_view(), name='about_me'),
    path('online_consultation/', OnlineConsultationView.as_view(), name='online_consultation'),
    path('offline_consultation/', OfflineConsultationView.as_view(), name='offline_consultation'),
    path('about_club/', AboutClubView.as_view(), name='about_club'),
]
