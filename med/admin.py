from django.contrib import admin
from med.models import Category, Article, Photo, Video, Answer, Comment

admin.site.register(Category)
admin.site.register(Article)
admin.site.register(Photo)
admin.site.register(Video)
admin.site.register(Answer)
admin.site.register(Comment)
