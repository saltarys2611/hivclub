from django.shortcuts import get_object_or_404, redirect
from django.views import View
from django.views.generic import ListView, UpdateView
from django.urls import reverse
from med.forms import CommentModerationForm, AnswerForm
from med.models import Comment
from accounts.views.helpers import AdminPermissionsView


class CommentDeleteView(AdminPermissionsView, View):
    model = Comment
    permission_required = 'delete_comment'

    def post(self, request, *args, **kwargs):
        content = ''
        content_id = 0
        comment = get_object_or_404(Comment, pk=self.kwargs.get('pk'))
        if comment.article_id is not None:
            content_id = comment.article_id
            content = 'article'
        if comment.photo_id is not None:
            content_id = comment.photo_id
            content = 'photo'
        if comment.video_id is not None:
            content_id = comment.video_id
            content = 'video'
        comment.is_deleted = True
        comment.save()
        return redirect(f'med:{content}_detail', pk=content_id)


class CommentListView(AdminPermissionsView, ListView):
    template_name = 'comments/comment_list.html'
    context_object_name = 'comments'
    ordering = ['created_at']

    def post(self, request, *args, **kwargs):
        print(self.request.POST)
        answer_form = AnswerForm(self.request.POST)
        if answer_form.is_valid():
            print('jdbkjs')
            answer = answer_form.save(commit=False)
            answer.author = self.request.user
            answer.save()

        return redirect('med:comment_list')

    def get_queryset(self):
        return Comment.objects.filter(is_question=False)


class QuestionListView(AdminPermissionsView, ListView):
    template_name = 'comments/questions_list.html'
    context_object_name = 'questions'
    ordering = ['created_at']
    permission_required = 'view_comment'

    def get_queryset(self):
        return Comment.objects.filter(is_question=True)


class CommentModerationView(AdminPermissionsView, UpdateView):
    model = Comment
    template_name = 'comments/comment_moderation.html'
    form_class = CommentModerationForm
    context_object_name = 'comment'
    permission_required = ['view_comment', 'change_comment', ]

    def post(self, request, *args, **kwargs):
        comment = get_object_or_404(Comment, pk=self.kwargs.get('pk'))
        comment.is_moderated = True
        comment.save()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('med:comment_list')

