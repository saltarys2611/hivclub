import logging


from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, ListView, DeleteView, DetailView, UpdateView

from helpers.color_distribution import color_distribution, COLORS
from helpers.date_time_encoder import DateTimeEncoder
from med.forms import PhotoForm, CommentForm, AnswerForm
from med.models import Photo, Category
import json
from accounts.views.helpers import AdminPermissionsView

from mixins.content_post import ContentPostMixin

logger = logging.getLogger('main')


class PhotoCreateView(AdminPermissionsView, CreateView):
    model = Photo
    form_class = PhotoForm
    template_name = 'photo/photo_create.html'
    success_url = reverse_lazy('med:photo_list')

    def get_context_data(self, **kwargs):
        context = super(PhotoCreateView, self).get_context_data()
        context['categories'] = Category.objects.all()
        return context

    def form_invalid(self, form):
        photo = {
            'image': self.request.FILES.get('image'),
            'title': self.request.POST.get('title'),
            'categories': [],
        }
        for category in self.request.POST.getlist('categories'):
            photo['categories'].append(int(category))

        context = self.get_context_data()
        context['photo'] = photo
        logger.warning('ошибка при публикации фото')
        return self.render_to_response(context)


class PhotoListView(LoginRequiredMixin, ListView, ContentPostMixin):
    template_name = 'photo/photo_list.html'
    context_object_name = 'photos'
    model = Photo
    ordering = ['-updated_at', ]

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['obj_with_colors'] = color_distribution(Category.objects.all())
        context['colors'] = COLORS
        context['qs_cat'] = json.dumps(list(Category.objects.values()), cls=DateTimeEncoder)
        return context


class PhotoDeleteView(AdminPermissionsView, DeleteView):
    template_name = 'photo/photo_delete.html'
    model = Photo
    context_object_name = 'photo'
    success_url = reverse_lazy('med:photo_list')


class PhotoUpdateView(AdminPermissionsView, UpdateView):
    template_name = 'photo/photo_update.html'
    form_class = PhotoForm
    model = Photo

    def get_context_data(self, **kwargs):
        context = super(PhotoUpdateView, self).get_context_data()
        context['categories'] = Category.objects.all()
        return context

    def get_success_url(self):
        return reverse('med:photo_list')


class PhotoDetailView(LoginRequiredMixin, DetailView):
    template_name = 'photo/photo_detail.html'
    model = Photo
    context_object_name = 'photo'

    def get(self, request, *args, **kwargs):
        photo = self.get_object()
        logger.info(f'Пользователь {self.request.user} был на странице и просмотрел видео: {photo.id}.')
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        comment_form = CommentForm(self.request.POST)
        answer_form = AnswerForm(self.request.POST)
        photo = get_object_or_404(Photo, pk=self.kwargs.get('pk'))

        if 'question_send' in self.request.POST:
            if comment_form.is_valid():
                question = comment_form.save(commit=False)
                question.author = self.request.user
                question.photo_id = photo.pk
                question.is_question = True
                if request.POST.get('is_anonymous'):
                    question.is_anonymous = True
                question.save()

        elif 'answer_send' in self.request.POST:
            if answer_form.is_valid():
                answer = answer_form.save(commit=False)
                answer.author = self.request.user
                answer.save()

        return redirect('med:photo_detail', pk=kwargs.get('pk'))
