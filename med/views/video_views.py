import logging

import json
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from helpers.color_distribution import color_distribution, COLORS
from helpers.date_time_encoder import DateTimeEncoder
from med.forms import VideoForm, CommentForm, AnswerForm
from med.models import Video, Category
import vimeo
from accounts.views.helpers import AdminPermissionsView
from mixins.content_post import ContentPostMixin

logger = logging.getLogger('main')


# List of unpublished videos. Sends a request to the Vimeo API,
# returns a list of videos that are not in the database. Available to admin.
# Adding videos from the list of unpublished videos to the database.


class AdminVideosList(AdminPermissionsView, CreateView):
    permission_required = ['add_video', ]
    template_name = 'video/video_list_admin.html'
    model = Video
    form = VideoForm
    fields = ['title', 'description', 'video_link', 'categories', 'picture']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        client = vimeo.VimeoClient(
            token='7cd9746491c6faea34760c6ce1ab19c2',
            key='b28a965f89a399303309e09106540176a9aea49f',
            secret='exQIToFUrBTbiQUHFgi6Fq0icQSLzfcNn2w/'
                   'prppKBOJPnA8cJQwIqAILLZPU7/idUOqh/'
                   'XZuzv5iCYggBX7o20a03yv2mI8O3OFuFDoeGP/bkIUY2zG7eCf8OipLBXz'
        )

        response = client.get('https://api.vimeo.com/me/videos/')
        video_links = Video.objects.filter().values('video_link')

        list_val = []

        for i in list(video_links):
            for key, val in i.items():
                list_val.append(val)

        context['videos'] = response.json().get('data')
        context['categories'] = Category.objects.all()
        context['video_links_saved'] = list_val
        return context

    def get_success_url(self):
        return reverse('med:videos')

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


# The view for the video detail page. Displays all information about the video,
# and there is also a form for a question.

class VideoDetailView(LoginRequiredMixin, DetailView):
    template_name = 'video/video_detail.html'
    context_object_name = 'video'
    model = Video

    def get(self, request, *args, **kwargs):
        video = self.get_object()
        logger.info(f'Пользователь {self.request.user} был на странице и просмотрел видео: {video.title}.')
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        comment_form = CommentForm(self.request.POST)
        answer_form = AnswerForm(self.request.POST)
        video = get_object_or_404(Video, pk=self.kwargs.get('pk'))

        if 'question_send' in self.request.POST:
            if comment_form.is_valid():
                question = comment_form.save(commit=False)
                question.author = self.request.user
                question.video_id = video.pk
                question.is_question = True
                if request.POST.get('is_anonymous'):
                    question.is_anonymous = True
                question.save()

        elif 'answer_send' in self.request.POST:
            if answer_form.is_valid():
                answer = answer_form.save(commit=False)
                answer.author = self.request.user
                answer.save()

        return redirect('med:video_detail', pk=kwargs.get('pk'))


# List of published videos from the database.

class Videos(LoginRequiredMixin, ListView, ContentPostMixin):
    model = Video
    template_name = 'video/videos.html'
    context_object_name = 'videos'

    def get_queryset(self):
        return Video.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context['obj_with_colors'] = color_distribution(Category.objects.all())
        context['colors'] = COLORS
        context['qs_cat'] = json.dumps(list(Category.objects.values()), cls=DateTimeEncoder)
        return context


# Video editing.

class VideoUpdate(AdminPermissionsView, UpdateView):
    permission_required = ['change_video', ]
    template_name = 'video/video_update.html'
    form_class = VideoForm
    model = Video
    context_object_name = 'v'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['categories'] = Category.objects.all()
        return context

    def get_success_url(self):
        return reverse('med:video_detail', kwargs={'pk': self.object.pk})


# Video Deleting from Database.

class VideoDeleteView(AdminPermissionsView, DeleteView):
    permission_required = ['delete_video', ]
    template_name = 'video/video_delete.html'
    model = Video
    context_object_name = 'video'
    success_url = reverse_lazy('med:videos')
