import logging
import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView

from helpers.color_distribution import color_distribution, COLORS
from helpers.date_time_encoder import DateTimeEncoder
from med.forms import AnswerForm
from med.models import Category, Article
from med.forms import ArticleForm, CommentForm
from accounts.views.helpers import AdminPermissionsView
from mixins.content_post import ContentPostMixin

logger = logging.getLogger('main')


class ArticleListView(LoginRequiredMixin, ListView, ContentPostMixin):
    template_name = 'article/articles_list.html'
    model = Article
    context_object_name = 'articles'
    ordering = ['-updated_at', ]

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['obj_with_colors'] = color_distribution(Category.objects.all())
        context['colors'] = COLORS
        context['qs_cat'] = json.dumps(list(Category.objects.values()), cls=DateTimeEncoder)
        return context


class ArticleDetailView(LoginRequiredMixin, DetailView):
    template_name = 'article/article_detail.html'
    model = Article
    context_object_name = 'article'

    def get(self, request, *args, **kwargs):
        article = self.get_object()
        logger.info(f'Пользователь {self.request.user} был на странице и просмотрел статью: {article.title}.')
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        comment_form = CommentForm(self.request.POST)
        answer_form = AnswerForm(self.request.POST)
        article = get_object_or_404(Article, pk=self.kwargs.get('pk'))

        if 'question_send' in self.request.POST:
            if comment_form.is_valid():
                question = comment_form.save(commit=False)
                question.author = self.request.user
                question.article_id = article.pk
                question.is_question = True
                if request.POST.get('is_anonymous'):
                    question.is_anonymous = True
                question.save()

        elif 'answer_send' in self.request.POST:
            if answer_form.is_valid():
                answer = answer_form.save(commit=False)
                answer.author = self.request.user
                answer.save()

        return redirect('med:article_detail', pk=kwargs.get('pk'))


class ArticleCreateView(AdminPermissionsView, CreateView):
    template_name = 'article/article_create.html'
    model = Article
    form_class = ArticleForm

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['categories'] = Category.objects.all()
        return context

    def form_invalid(self, form):
        article = {
            'picture': self.request.FILES.get('picture'),
            'categories': [],
        }
        for field in self.request.POST:
            if field == 'categories':
                for category in self.request.POST.getlist('categories'):
                    article['categories'].append(int(category))
            else:
                article[field] = self.request.POST.get(field)

        context = self.get_context_data()
        context['article'] = article
        return self.render_to_response(context)

    def get_success_url(self):
        return reverse('med:article_detail', kwargs={'pk': self.object.pk})


class ArticleDeleteView(AdminPermissionsView, DeleteView):
    template_name = 'article/article_delete.html'
    model = Article
    context_object_name = 'article'
    success_url = reverse_lazy('med:articles')


class ArticleUpdateView(AdminPermissionsView, UpdateView):
    permission_required = ['change_article', ]
    template_name = 'article/article_update.html'
    form_class = ArticleForm
    model = Article
    context_object_name = 'article'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['categories'] = Category.objects.all()
        return context

    def get_success_url(self):
        return reverse('med:article_detail', kwargs={'pk': self.object.pk})
