from django.shortcuts import render
from django.views import View


class AboutMeView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'about_me.html')


class OnlineConsultationView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'online_consultation.html')


class OfflineConsultationView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'offline_consultation.html')


class AboutClubView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'about_club.html')
