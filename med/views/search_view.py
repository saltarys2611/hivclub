from django.http import JsonResponse
from med.models import Category


def search_view(request):
    if request.is_ajax():
        data = []
        response = None
        search_value = request.POST.get('search_value')
        ph = Category.objects.filter(title__icontains=search_value)
        if len(ph) > 0 and len(search_value) > 0:
            if ph:
                for pos in ph:
                    item = {
                        'pk': pos.pk,
                        'name': pos.title,
                        'content_url': 'photo'
                    }
                    data.append(item)
            response = data
        else:
            response = 'Not found ...'
        return JsonResponse({'data': response})
    return JsonResponse({})
