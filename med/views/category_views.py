import json
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from med.models import Category, Article, Photo, Video
from med.forms import CategoryForm
from med.views.photo_views import DateTimeEncoder
from helpers.color_distribution import color_distribution, COLORS
from accounts.views.helpers import AdminPermissionsView


class CategoryListView(ListView):
    template_name = 'categories/categories_list.html'
    model = Category
    context_object_name = 'categories'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['obj_with_colors'] = color_distribution(Category.objects.all())
        context['colors'] = COLORS
        context['qs_cat'] = json.dumps(list(Category.objects.values()), cls=DateTimeEncoder)
        return context

    def post(self, *args, **kwargs):
        data = []
        response = None
        not_found = None
        context = {}
        html = ''
        category = self.request.POST.get('category')
        content_type = self.request.POST.get('content_type')

        if content_type == 'articles':
            data = Article.objects.filter(categories=category)
            html = 'article/articles_list.html'
        elif content_type == 'photos':
            data = Photo.objects.filter(categories=category)
            html = 'photo/photo_list.html'
        elif content_type == 'videos':
            data = Video.objects.filter(categories=category)
            html = 'video/videos.html'

        if len(data) > 0:
            response = data
        else:
            not_found = 'Not found...'

        context['obj_with_colors'] = color_distribution(Category.objects.all())
        context['colors'] = COLORS
        context['qs_cat'] = json.dumps(list(Category.objects.values()), cls=DateTimeEncoder)
        context['data'] = data
        context['not_found'] = not_found
        context['search_cat'] = Category.objects.get(id=category)
        context['search_categories'] = True

        return render(self.request, html, context=context)


class CategoryCreateView(AdminPermissionsView, CreateView):
    template_name = 'categories/create_category.html'
    model = Category
    form_class = CategoryForm

    def get_success_url(self):
        return reverse('med:categories')


class CategoryDeleteView(AdminPermissionsView, DeleteView):
    model = Category
    context_object_name = 'category'
    success_url = reverse_lazy('med:categories')


class CategoryEditView(AdminPermissionsView, UpdateView):
    template_name = 'categories/update_category.html'
    model = Category
    form_class = CategoryForm
    context_object_name = 'category'

    def get_success_url(self):
        return reverse('med:categories')


class CategoryArticleListView(DetailView):
    template_name = 'categories/article_category_list.html'
    model = Category
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articles'] = Article.objects.filter(categories=self.object)
        return context


class CategoryPhotoListView(DetailView):
    template_name = 'categories/photo_category_list.html'
    model = Category
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['photos'] = Photo.objects.filter(categories=self.object)
        return context


class CategoryVideoListView(DetailView):
    template_name = 'categories/video_category_list.html'
    model = Category
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['video'] = Video.objects.filter(categories=self.object)
        return context
