from django.contrib.auth import get_user_model
from django.db import models
from embed_video.fields import EmbedVideoField


class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name='Название категории')
    is_main = models.BooleanField(default=False)
    main_category = models.ForeignKey(
        'self', null=True, blank=True, related_name='subcategories', on_delete=models.CASCADE)
    is_paid = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ['-title', ]


class Content(models.Model):
    title = models.CharField(max_length=1500, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата редактирования')

    class Meta:
        abstract = True


class Video(Content):
    picture = models.TextField(null=True, blank=True, verbose_name='Картинка')
    description = models.TextField(max_length=2000, null=True, blank=True, verbose_name='Описание')
    video_link = EmbedVideoField(max_length=10000, verbose_name='Ссылка видео', unique=True)
    categories = models.ManyToManyField('med.Category', related_name='video_categories')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created_at', ]


class Article(Content):
    subtitle = models.TextField(max_length=1000, null=True, verbose_name='Подзаголовок')
    picture = models.ImageField(upload_to='article_pics', verbose_name='Картинка')
    body = models.TextField(verbose_name='Тело статьи')
    categories = models.ManyToManyField('med.Category', related_name='article_categories')

    def __str__(self):
        return f'{self.title}, {self.created_at}'


class Photo(Content):
    image = models.ImageField(null=False, blank=False, upload_to='image_content', verbose_name='Фото')
    categories = models.ManyToManyField('med.Category', related_name='photo_categories')

    def __str__(self):
        return self.title


class Comment(models.Model):
    article = models.ForeignKey(Article, null=True, blank=True, related_name='comment', on_delete=models.CASCADE)
    photo = models.ForeignKey(Photo, null=True, blank=True, related_name='comment', on_delete=models.CASCADE)
    video = models.ForeignKey(Video, null=True, blank=True, related_name='comment', on_delete=models.CASCADE)
    author = models.ForeignKey(get_user_model(), related_name='comment', on_delete=models.CASCADE,
                               verbose_name='Автор')
    body = models.TextField(max_length=2000, null=False, blank=False, verbose_name='Коментарий')
    is_anonymous = models.BooleanField(default=False)
    is_question = models.BooleanField(default=False)
    is_review = models.BooleanField(default=False)
    is_moderated = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    def __str__(self):
        return f'{self.id}. {self.author}'

    class Meta:
        ordering = ['-created_at', ]


class Answer(models.Model):
    comment = models.ForeignKey(Comment, related_name='answer_comment',
                                on_delete=models.CASCADE, verbose_name='Ответ')
    author = models.ForeignKey(get_user_model(), related_name='answer', on_delete=models.CASCADE,
                               verbose_name='Автор')
    body = models.TextField(max_length=2000, null=False, blank=False, verbose_name='Ответ')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    def __str__(self):
        return f'{self.id} {self.author}'
