from itertools import zip_longest

COLORS = ['#E9AED2', '#66C3D0', '#C1B3D1', '#82E0D5', '#D7A1C2', '#A2D9F7', '#B2A2C6', '#7ACFC8', '#F5B2B6', '#FCD5A6']


def color_distribution(objects: list) -> list:
    i = 0
    general_list = zip_longest(objects, COLORS)
    result = []

    for obj, color in general_list:
        if not color:
            color = COLORS[i]
            i += 1

        if i >= len(COLORS):
            i = 0

        result.append((obj, color))

    return result
