from rest_framework import serializers
from django.contrib.auth.models import User

from accounts.models import Profile
from med.models import Comment, Answer


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'avatar',
            'is_admin',
        ]


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = [
            'id',
            'is_superuser',
            'username',
            'first_name',
            'last_name',
            'profile',
        ]


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'id',
            'article',
            'photo',
            'video',
            'author',
            'body',
            'is_anonymous',
            'is_question',
            'is_review',
            'is_moderated',
            'is_deleted',
            'created_at',
        ]
        read_only = ['author']


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = [
            'body',
            'comment',
            'author',
            'created_at',
        ]
