from django.urls import path, include
from rest_framework import routers

from api.views import QuestionsViewSet, UsersViewSet, AnswerViewSet

router = routers.DefaultRouter()
router.register('questions', QuestionsViewSet)
router.register('users', UsersViewSet)
router.register('answers', AnswerViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
