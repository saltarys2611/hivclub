from rest_framework import viewsets
from django.contrib.auth.models import User

from api.serialisers import CommentSerializer, UserSerializer, AnswerSerializer
from med.models import Comment, Answer


class QuestionsViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.filter(is_question=True)
    serializer_class = CommentSerializer


class UsersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
